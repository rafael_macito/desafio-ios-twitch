//
//  UIView.swift
//  CheckinCheckout
//
//  Created by Rafael Macito on 27/02/2018.
//  Copyright © 2018 Madeinweb. All rights reserved.
//

import UIKit

extension UIView {
    
    @IBInspectable var borderColor: UIColor? {
        set {
            layer.borderColor = newValue?.cgColor
        }
        get {
            guard let color = layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: color)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
    
    var currentFirstResponder: UIView? {
        if isFirstResponder {
            return self
        }
        
        for view in self.subviews {
            if let responder = view.currentFirstResponder {
                return responder
            }
        }
        
        return nil
    }
    
    var firstScrollView: UIView? {
        guard let superview = superview else { return nil }
        
        if superview.isKind(of: UIScrollView.self) {
            return superview
        } else {
            return superview.firstScrollView
        }
    }
    
    var visibleFrame: CGRect? {
        guard let superview = superview else { return nil }
        return frame.intersection(superview.bounds)
    }
}
