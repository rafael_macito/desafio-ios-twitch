//
//  Game.swift
//  TwitchTeste
//
//  Created by Rafael Macito on 18/04/2018.
//  Copyright © 2018 Macito. All rights reserved.
//

import Foundation

struct TopGameResponse: Decodable {
    
    var total: Int?
    var topList: [GameInfo]?
    
    enum CodingKeys: String, CodingKey {
        case total = "_total"
        case topList = "top"
    }
}

struct GameInfo: Decodable {

    var viewers: Int?
    var game: Game?
}

struct Game: Decodable {
    
    var name: String?
    var box: Image?
    var popularity: Int?
}

struct Image: Decodable {
    
    var large: URL?
    var medium: URL?
    var small: URL?
    var custom: URL?
    
    enum CodingKeys: String, CodingKey {
        case large
        case medium
        case small
        case template
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        large = try? values.decode(URL.self, forKey: .large)
        medium = try? values.decode(URL.self, forKey: .medium)
        small = try? values.decode(URL.self, forKey: .small)

        if var template = try? values.decode(String.self, forKey: .template) {
            template = template.replacingOccurrences(of: "{width}", with: "400")
            template = template.replacingOccurrences(of: "{height}", with: "500")
            
            custom = URL(string: template)
        }
    }
}
