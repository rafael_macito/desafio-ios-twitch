//
//  BaseInteractor.swift
//  Arch
//
//  Created by Rafael Macito on 26/01/2018.
//  Copyright © 2018 Madeinweb. All rights reserved.
//

import Foundation

protocol BaseInteractorDelegate: class {
    func error(_ title: String?, _ error: String)
}

protocol NetworkConnectionVerifier {
    
    func checkInternetConnection() -> Bool
}

extension NetworkConnectionVerifier {
    
    func checkInternetConnection() -> Bool {
        guard let status = Network.reachability?.status else { return false }
        switch status {
        case .unreachable:
            return false
            
        default:
            return true
        }
    }
}
