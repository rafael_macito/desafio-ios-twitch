//
//  APIDataManager.swift
//  Arch
//
//  Created by Rafael Macito on 26/01/2018.
//  Copyright © 2018 Madeinweb. All rights reserved.
//

import Foundation

enum RequestType: String {
    
    case get    = "GET"
    case post   = "POST"
    case patch  = "PATCH"
    case delete = "DELETE"
}

enum Endpoint: String {
    
    case topGames = "/kraken/games/top"
}

protocol APIDataManager {
    
    var apiKey: String { get }
    var baseURL: String { get }
    func request<D: Decodable>(endpoint: Endpoint,
                               method: RequestType,
                               parameters: inout [String: Any],
                               success: @escaping (_ response: D?) -> Void,
                               failure: @escaping (_ errorCode: Int?, _ errorMsg: String?) -> Void)
}

extension APIDataManager {
    
    var apiKey: String {
        return "r155ve0vt48axvcx9a8lxvrij7432w"
    }
    
    var baseURL: String {
        return "https://api.twitch.tv"
    }
    
    func request<D: Decodable>(endpoint: Endpoint,
                               method: RequestType,
                               parameters: inout [String: Any],
                               success: @escaping (_ response: D) -> Void,
                               failure: @escaping (_ errorCode: Int?, _ errorMsg: String?) -> Void) {
        var url = baseURL + endpoint.rawValue
        let session = URLSession.shared
        let request = NSMutableURLRequest()
        
        request.httpMethod = method.rawValue
        
        parameters.updateValue(apiKey, forKey: "client_id")
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        url += parameters.buildQueryString()
        print(url)
        request.url = URL(string: url)
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            DispatchQueue.main.async {
                guard error == nil, let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode == 200 else {
                    failure(nil, error?.localizedDescription ?? "")

                    return
                }

                guard let data = data, !data.isEmpty else {
                    failure(nil, nil)
                    
                    return
                }
                
                do {
                    success(try JSONDecoder().decode(D.self, from: data))
                } catch let error as NSError {
                    failure(statusCode, "Parse error: " + String(describing: error))
                }
            }
        })
        
        task.resume()
    }
}
