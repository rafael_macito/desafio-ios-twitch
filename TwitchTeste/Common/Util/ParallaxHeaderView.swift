//
// ParallaxHeaderView.swift
//  CheckinCheckout
//
//  Created by Rafael Macito on 06/02/2018.
//  Copyright © 2018 Madeinweb. All rights reserved.
//

import UIKit
import Kingfisher

class ParallaxHeaderView: UIView {
    var headerImgView: UIImageView?
    
    var heightLayoutConstraint = NSLayoutConstraint()
    var bottomLayoutConstraint = NSLayoutConstraint()
    
    var containerView = UIView()
    var containerLayoutConstraint = NSLayoutConstraint()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.backgroundColor = UIColor.clear
        self.addSubview(containerView)
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[containerView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["containerView": containerView]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[containerView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["containerView": containerView]))
        containerLayoutConstraint = NSLayoutConstraint(item: containerView, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1.0, constant: 0.0)
        self.addConstraint(containerLayoutConstraint)
        
        headerImgView = UIImageView()
        guard let headerImgView = headerImgView else { return }
        
        headerImgView.translatesAutoresizingMaskIntoConstraints = false
        headerImgView.clipsToBounds = true
        headerImgView.contentMode = .scaleAspectFill
        headerImgView.image = UIImage(named: "house_placeholder")
        containerView.addSubview(headerImgView)
        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[headerImgView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["headerImgView": headerImgView]))
        bottomLayoutConstraint = NSLayoutConstraint(item: headerImgView, attribute: .bottom, relatedBy: .equal, toItem: containerView, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        containerView.addConstraint(bottomLayoutConstraint)
        heightLayoutConstraint = NSLayoutConstraint(item: headerImgView, attribute: .height, relatedBy: .equal, toItem: containerView, attribute: .height, multiplier: 1.0, constant: 0.0)
        containerView.addConstraint(heightLayoutConstraint)
    }
    
    func loadImage(from url: URL?, placeHolder: UIImage? = nil) {
        headerImgView?.kf.indicatorType = .activity
        headerImgView?.kf.setImage(with: url,
                                   placeholder: placeHolder,
                                   options: [.transition(.fade(0.3))])
    }

    func scrollViewDidScroll(scrollView: UIScrollView) {
        containerLayoutConstraint.constant = scrollView.contentInset.top
        let offsetY = -(scrollView.contentOffset.y + scrollView.contentInset.top)
        containerView.clipsToBounds = offsetY <= 0
        bottomLayoutConstraint.constant = offsetY >= 0 ? 0 : -offsetY / 2
        heightLayoutConstraint.constant = max(offsetY + scrollView.contentInset.top, scrollView.contentInset.top)
    }
}
