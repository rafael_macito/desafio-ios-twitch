//
//  GameListRouter.swift
//  TwitchTeste
//
//  Created by Rafael Macito on 18/04/2018.
//  Copyright © 2018 Macito. All rights reserved.
//

import Foundation

class GameListRouter: BaseRouter {
    
    static func show() {
        let viewController = GameListViewController(nibName: "GameListView", bundle: Bundle.main)
        let presenter = GameListPresenter()
        let interactor = GameListInteractor()
        
        viewController.presenter = presenter
        presenter.delegate = viewController
        
        presenter.interactor = interactor
        interactor.delegate = presenter
        
        pushViewController(controller: viewController, animation: .fade)
    }
}
