//
//  GameListCell.swift
//  TwitchTeste
//
//  Created by Rafael Macito on 18/04/2018.
//  Copyright © 2018 Macito. All rights reserved.
//

import UIKit
import Kingfisher

class GameListCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var rankingLabel: UILabel?
    @IBOutlet weak var boxImageView: UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 5
        layer.shadowOffset = CGSize(width: 5, height: 2)
        
        backgroundColor = .clear
    }
    
    func loadUI(_ gameInfo: GameInfo) {
        nameLabel?.text = gameInfo.game?.name
        rankingLabel?.text = "# " + "\(gameInfo.game?.popularity ?? 0)"
        
        boxImageView?.kf.indicatorType = .activity
        boxImageView?.kf.setImage(with: gameInfo.game?.box?.medium,
                                  placeholder: UIImage(named: "twitch_box_placeholder"),
                                  options: [.transition(.fade(0.3))])
    }
}
