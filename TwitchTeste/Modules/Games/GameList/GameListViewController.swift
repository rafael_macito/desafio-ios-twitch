//
//  GameListViewController.swift
//  TwitchTeste
//
//  Created by Rafael Macito on 18/04/2018.
//  Copyright © 2018 Macito. All rights reserved.
//

import UIKit
import Hero

class GameListViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView?
    
    var presenter: GameListPresenter?
    var gameInfos: [GameInfo] = []
    var loading: Bool = true {
        didSet {
            if #available(iOS 10.0, *) {
                tableView?.refreshControl?.isEnabled = loading
            }
        }
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor(red: 99/255, green: 68/255, blue: 169/255, alpha: 1)
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configTableView()
        
        presenter?.loadTopGames()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configNavBar()
    }
    
    override func loading(_ loading: Bool) {
        if loading {
            if !refreshControl.isRefreshing {
                addActivityFooter()
            }
        } else {
            if !refreshControl.isRefreshing {
                tableView?.tableFooterView = UIView()
            }
        }
    }
    
    // MARK: Custom
    
    func configTableView() {
        tableView?.tableHeaderView = UIView()
        tableView?.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 15))
        
        tableView?.register(UINib(nibName: "GameListCell", bundle: Bundle.main), forCellReuseIdentifier: "GameListCell")
        tableView?.rowHeight = 140
        
        tableView?.addSubview(refreshControl)
    }
    
    func configNavBar() {
        let imageView = UIImageView(image: UIImage(named: "logo_twitch"))
        imageView.frame = CGRect(x: 0, y: 0, width: 150, height: 30)
        imageView.widthAnchor.constraint(equalToConstant: 125).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        self.navigationItem.titleView = imageView
        
        navigationController?.navigationBar.isHidden = false
        
        if let font = UIFont(name: "KelsonSans-Regular", size: 25) {
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 99/255,
                                                                                                                      green: 68/255,
                                                                                                                      blue: 169/255,
                                                                                                                      alpha: 1),
                                                                       NSAttributedStringKey.font: font]
        }

        let logoView = UIImageView(image: #imageLiteral(resourceName: "ic_twitch"))
        logoView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        logoView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        logoView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: logoView)]
        navigationItem.hidesBackButton = true
    }
    
    func addActivityFooter() {
        let activiyIndicator = UIActivityIndicatorView()
        activiyIndicator.color = UIColor(red: 99/255, green: 68/255, blue: 169/255, alpha: 1)
        activiyIndicator.startAnimating()
        
        let activityView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 50))
        activityView.addSubview(activiyIndicator)
        
        activiyIndicator.frame.size = CGSize(width: 50, height: 50)
        activiyIndicator.translatesAutoresizingMaskIntoConstraints = false
        activiyIndicator.centerXAnchor.constraint(equalTo: activityView.centerXAnchor).isActive = true
        activiyIndicator.centerYAnchor.constraint(equalTo: activityView.centerYAnchor).isActive = true
        activiyIndicator.widthAnchor.constraint(equalToConstant: 50).isActive = true
        activiyIndicator.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        activityView.layoutIfNeeded()
        
        tableView?.tableFooterView = activityView
    }
    
    @objc func handleRefresh() {
        gameInfos = []
        
        tableView?.reloadData()
        
        presenter?.reloadTopGames()
        
        refreshControl.endRefreshing()
    }
}

extension GameListViewController: GameListPresenterDelegate {
    
    // MARK: GameListPresenterDelegate
    
    func loadedTopGames(_ topList: [GameInfo]) {
        let last = gameInfos.count
        
        gameInfos.append(contentsOf: topList)
        
        guard let contentOffset = tableView?.contentOffset else { return }
        
        tableView?.beginUpdates()
            
        var indexPaths: [IndexPath] = []
        for row in last...(gameInfos.count - 1) {
            indexPaths.append(IndexPath(row: row, section: 0))
        }
        
        tableView?.insertRows(at: indexPaths, with: .fade)
        
        tableView?.setContentOffset(contentOffset, animated: false)
        
        tableView?.endUpdates()
        
        loading = false
    }
}

extension GameListViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gameInfos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GameListCell", for: indexPath) as? GameListCell else { return UITableViewCell() }
        
        cell.loadUI(gameInfos[indexPath.row])
        cell.boxImageView?.hero.id = "GameListCell_" + "\(indexPath.row)"
        
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row >= 0, indexPath.row < gameInfos.count else { return }
        
        let id = (tableView.cellForRow(at: indexPath) as? GameListCell)?.boxImageView?.hero.id
        presenter?.goToGameDetail(gameInfos[indexPath.row], imageViewId: id)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0.3
        
        var y: CGFloat
        let translation = tableView.panGestureRecognizer.translation(in: view)
        if translation.y > 0 {
            y = -75
        } else if translation.y < 0 {
            y = 75
        } else {
            y = 0
        }
        
        cell.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, y, 0)
        
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut, .allowUserInteraction], animations: {
            cell.alpha = 1
            cell.layer.transform = CATransform3DIdentity
        }, completion: nil)
    }
}

extension GameListViewController: UIScrollViewDelegate {
    
    // MARK: UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard !gameInfos.isEmpty, !loading else { return }
        
        if scrollView.contentSize.height - scrollView.contentOffset.y - view.frame.height - 67 < 0 {
            loading = true
            presenter?.loadTopGames()
        }
    }
}
