//
//  GameListPresenter.swift
//  TwitchTeste
//
//  Created by Rafael Macito on 18/04/2018.
//  Copyright © 2018 Macito. All rights reserved.
//

import Foundation

protocol GameListPresenterDelegate: BasePresenterDelegate {
    func loadedTopGames(_ topList: [GameInfo])
}

class GameListPresenter {

    var interactor: GameListInteractor?
    weak var delegate: GameListPresenterDelegate?

    var page: Int = 0
    var offSet: Int = 50
    var total: Int?
    
    func goToGameDetail(_ gameInfo: GameInfo, imageViewId: String?) {
        GameDetailRouter.show(gameInfo: gameInfo, imageViewId: imageViewId)
    }
    
    func loadTopGames() {
        if let total = total, page > total { return }
        
        delegate?.loading(true)
        
        interactor?.loadTopGames(page: page, offSet: offSet)
    }
    
    func reloadTopGames() {
        page = 0
        
        // Esse delay foi feito por que o UIRefreshControl buga se for removido muito rapido
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            self?.loadTopGames()
        }
    }
}

extension GameListPresenter: GameListInteractorDelegate {
    func loadedTopGames(_ total: Int, _ topList: [GameInfo]) {
        delegate?.loading(false)
        
        self.total = total
        page += offSet
        
        delegate?.loadedTopGames(topList)
    }
    
    func error(_ title: String?, _ error: String) {
        delegate?.loading(false)
        
        delegate?.error(title, error)
    }
}
