//
//  GameListInteractor.swift
//  TwitchTeste
//
//  Created by Rafael Macito on 18/04/2018.
//  Copyright © 2018 Macito. All rights reserved.
//

import Foundation

protocol GameListInteractorDelegate: BaseInteractorDelegate {
    func loadedTopGames(_ total: Int, _ topList: [GameInfo])
}

class GameListInteractor: APIDataManager {
    
    weak var delegate: GameListInteractorDelegate?
    
    func loadTopGames(page: Int, offSet: Int) {
        var parameters = ["limit": offSet,
                          "offset": page] as [String: Any]
        
        request(endpoint: .topGames, method: .get, parameters: &parameters, success: { [weak self] (response: TopGameResponse) in
            if let total = response.total, let topList = response.topList {
                self?.delegate?.loadedTopGames(total, topList)
            } else {
                self?.delegate?.error(nil, "Empty Data")
            }
        }, failure: { [weak self] (errorCode, statusMessage) in
            self?.delegate?.error(String(describing: errorCode), statusMessage ?? "Unkown error")
        })
    }
}
