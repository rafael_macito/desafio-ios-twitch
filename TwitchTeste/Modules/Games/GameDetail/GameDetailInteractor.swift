//
//  GameDetailInteractor.swift
//  TwitchTeste
//
//  Created by Rafael Macito on 18/04/2018.
//  Copyright © 2018 Macito. All rights reserved.
//

import Foundation

protocol GameDetailInteractorDelegate: class {
    
}

class GameDetailInteractor {
    
    weak var delegate: GameDetailInteractorDelegate?
}
