//
//  GameDetailRouter.swift
//  TwitchTeste
//
//  Created by Rafael Macito on 18/04/2018.
//  Copyright © 2018 Macito. All rights reserved.
//

import Foundation
import Hero

class GameDetailRouter: BaseRouter {
    
    static func show(gameInfo: GameInfo, imageViewId: String?) {
        let viewController = GameDetailViewController(nibName: "GameDetailView", bundle: Bundle.main)
        let presenter = GameDetailPresenter()
        let interacotr = GameDetailInteractor()
        
        viewController.headerImgViewId = imageViewId
        
        presenter.gameInfo = gameInfo
        
        viewController.presenter = presenter
        presenter.delegate = viewController
        
        presenter.interactor = interacotr
        interacotr.delegate = presenter
        
        pushViewController(controller: viewController)
    }
}
