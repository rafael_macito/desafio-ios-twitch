//
//  GameDetailViewController.swift
//  TwitchTeste
//
//  Created by Rafael Macito on 18/04/2018.
//  Copyright © 2018 Macito. All rights reserved.
//

import UIKit
import Kingfisher

class GameDetailViewController: BaseViewController {
    
    var presenter: GameDetailPresenter?
    
    @IBOutlet weak var parallaxHeaderView: ParallaxHeaderView?
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var viewersLabel: UILabel?
    
    var headerImgViewId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        parallaxHeaderView?.headerImgView?.hero.id = headerImgViewId
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configNavBar()
        
        presenter?.loadUIIfNedeed()
    }
    
    // MARK: Custom
    
    func configNavBar() {
        title = "Top Games"
        
        navigationController?.navigationBar.isHidden = false
        
        let backButton = UIButton(type: .custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        backButton.setImage(UIImage(named: "ic_back_arrow"), for: .normal)
        backButton.addTarget(self, action: #selector(dismissViewController), for: .touchUpInside)
        backButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: backButton)]
        
        let logoView = UIImageView(image: #imageLiteral(resourceName: "ic_twitch"))
        logoView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        logoView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        logoView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: logoView)]
    }
    
    @objc func dismissViewController() {
        BaseRouter.dismissViewController()
    }
}

extension GameDetailViewController: GameDetailPresenterDelegate {
    func loadUI(_ gameInfo: GameInfo) {
        nameLabel?.text = gameInfo.game?.name
        viewersLabel?.text = "\(gameInfo.viewers ?? 0)" + " viewers"
        
        parallaxHeaderView?.loadImage(from: gameInfo.game?.box?.custom, placeHolder: UIImage(named: "twitch_box_placeholder"))
    }
}

extension GameDetailViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        parallaxHeaderView?.scrollViewDidScroll(scrollView: scrollView)
    }
}
