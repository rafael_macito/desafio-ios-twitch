//
//  GameDetailPresenter.swift
//  TwitchTeste
//
//  Created by Rafael Macito on 18/04/2018.
//  Copyright © 2018 Macito. All rights reserved.
//

import Foundation

protocol GameDetailPresenterDelegate: BasePresenterDelegate {
    func loadUI(_ gameInfo: GameInfo)
}

class GameDetailPresenter {
    
    var interactor: GameDetailInteractor?
    weak var delegate: GameDetailPresenterDelegate?
    var gameInfo: GameInfo?
    
    func loadUIIfNedeed() {
        guard let gameInfo = gameInfo else { return }
        
        delegate?.loadUI(gameInfo)
    }
}

extension GameDetailPresenter: GameDetailInteractorDelegate {
    
}
