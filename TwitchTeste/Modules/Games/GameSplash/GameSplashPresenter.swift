//
//  GameSplashPresenter.swift
//  TwitchTeste
//
//  Created by Rafael Macito on 18/04/2018.
//  Copyright © 2018 Macito. All rights reserved.
//

import Foundation

class GameSplashPresenter {
    
    func goToGameList() {
        GameListRouter.show()
    }
}
