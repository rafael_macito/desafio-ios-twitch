//
//  AppDelegate.swift
//  BaseProject
//
//  Created by Rafael Macito on 19/03/2018.
//  Copyright © 2018 Madeinweb. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // MARK: Reachability

        do {
            Network.reachability = try Reachability(hostname: "www.google.com")
            do {
                try Network.reachability?.start()
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
        
        // MARK: SetRoot
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        BaseRouter.setRootView()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
}
